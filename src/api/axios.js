import axios from 'axios'

export const api = axios.create({
    baseURL: 'http://54.241.228.34:8080/api/v1/'
})

export const getPostsPage = async (pageNum = 1) => {
    const response = await api.get(`lab/getAllLab?page=${pageNum}&size=5`)
    console.log(" Here is the post page : ",response.data.data )
    return response.data.data
}